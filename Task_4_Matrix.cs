using System;

namespace Program
{
	class MyMatrix
	{	
		#region Fields
		
		private double[][] _elements;
        private int _col;
        private int _row;        
		
		#endregion
		
		#region Constructors

		public MyMatrix(int row, int column)
        {
            _col = column;
            _row = row;
			_elements = new double[row][];
			for(var i = 0; i < row; i++)
			{
				_elements[i] = new double[column];
			}
        }

        public MyMatrix(MyMatrix ToCopy)
        {            
		    _row = ToCopy._row;
            _col = ToCopy._col;						
			_elements = ToCopy._elements;
        }
		
		#endregion
		
		#region Properties
		
		public int Rows
		{
			get
			{
				return _row;
			}
		}
		
		public int Columns
		{
			get
			{
				return _col;
			}
		}
		
		public double this[int row, int column]
        {
			get
			{
				if(row < 0 || row >= _row || column < 0 || column >= _col)
				{
					throw new IndexOutOfRangeException();
				}
				else
				{
					return _elements[row][column];
				}				
			}
			set
			{
				if(row < 0 || row >= _row || column < 0 || column >= _col)
				{
					throw new IndexOutOfRangeException();
				}
				else
				{
					_elements[row][column] = value;
				}
			}
        }		
	
		#endregion
		
		#region Methods		
	
		public static bool Equals(MyMatrix mat1, MyMatrix mat2)
		{
			if(mat1.Rows != mat2.Rows || mat1.Columns != mat2.Columns)
			{
				throw new ArgumentException("Matrixes have different number of rows or columns");
			}
			else
			{
				for(var i = 0; i < mat1.Rows; i++)
				{
					for(var j = 0; j < mat1.Columns; j++)
					{
						if(mat1[i, j] != mat2[i, j])
							return false;
					}
				}
				
				return true;
			}
		}
		
		public void MultiplyByNumber(double number)
		{
			for(var i = 0; i < this.Rows; i++)
			{
				for(var j = 0; j < this.Columns; j++)
				{
					_elements[i][j] *= number;
				}
			}
		}
		
		public override string ToString()
		{
			string result = "";
			
			for(var i = 0; i < this.Rows; i++)
			{
				for(var j = 0; j < this.Columns; j++)
				{
					result += String.Format("{0} ", _elements[i][j]);
				}
				
				result += Environment.NewLine;
			}
			
			return result;
		}
		
		public static MyMatrix Transpose(MyMatrix mat1)
		{
			MyMatrix resultMatrix = new MyMatrix(mat1.Rows, mat1.Columns);
			
			for(var i = 0; i < mat1.Rows; i++)
			{
				for(var j = 0; j < mat1.Columns; j++)
				{
					resultMatrix[i, j] = mat1[j, i];
				}						
			}
			
			return resultMatrix;
		}
		
		public static MyMatrix SubMatrix(MyMatrix mat1, int[] rowIndexes, int[] columnIndexes)
		{
			MyMatrix resultMatrix = new MyMatrix(rowIndexes.Length, columnIndexes.Length);
			
			for(var i = 0; i < rowIndexes.Length; i++)
			{
				for(var j = 0; j < columnIndexes.Length; j++)
				{
					resultMatrix[i, j] = mat1[rowIndexes[i], columnIndexes[j]];
				}						
			}
			
			return resultMatrix;
		}
		
		#endregion	

		#region Operators
		
		public static MyMatrix operator +(MyMatrix mat1, MyMatrix mat2)
		{
			if(mat1.Rows != mat2.Rows || mat1.Columns != mat2.Columns)
			{
				throw new ArgumentException("Matrixes have different number of rows or columns");
			}
			else
			{
				for(var i = 0; i < mat1.Rows; i++)
				{
					for(var j = 0; j < mat1.Columns; j++)
					{
						mat1[i, j] += mat2[i, j];
					}
				}
				
				return mat1;
			}
		}
		
		public static MyMatrix operator -(MyMatrix mat1, MyMatrix mat2)
		{
			if(mat1.Rows != mat2.Rows || mat1.Columns != mat2.Columns)
			{
				throw new ArgumentException("Matrixes have different number of rows or columns");
			}
			else
			{
				for(var i = 0; i < mat1.Rows; i++)
				{
					for(var j = 0; j < mat1.Columns; j++)
					{
						mat1[i, j] -= mat2[i, j];
					}
				}
				
				return mat1;
			}
		}
		
		public static MyMatrix operator *(MyMatrix mat1, MyMatrix mat2)
		{
			if(mat1.Columns != mat2.Rows)
			{
				throw new ArgumentException("First and second matrix have different number of culumns and rows");
			}
			else
			{
				MyMatrix resultMatrix = new MyMatrix(mat1.Columns, mat2.Rows);
				
				for(var i = 0; i < mat1.Rows; i++)
				{
					for(var j = 0; j < mat2.Columns; j++)
					{
						for(var k = 0; k < mat2.Rows; k++)
						{
							resultMatrix[i, j] += mat1[i, k] * mat2[k, j];	
						}						
					}
				}
				
				return resultMatrix;
			}		
		}
		
		#endregion
	}
	
	class MatrixTest
	{
		static void Main()
		{
			MyMatrix mat1 = new MyMatrix(4, 4);			
			mat1[0, 0] = 1;
			mat1[1, 1] = 2;
			mat1[2, 2] = 1;
			mat1[3, 3] = 1;
			
			MyMatrix mat2 = new MyMatrix(4, 4);
			mat2[1, 3] = 1;
			mat2[2, 2] = 2;
			mat2[1, 1] = 2;
			mat2[0, 0] = 1;
			mat2[0, 3] = 1;											
		
			mat2.MultiplyByNumber(2);
			Console.WriteLine("All matrix elements were multiplied by 2:");
			Console.WriteLine(mat1.ToString());
			
			mat1 = mat1 * mat2;
			Console.WriteLine("Matrix1 * matrix2:");
			Console.WriteLine(mat1.ToString());				
			
			Console.WriteLine("Is matrix1 equal matrix2? {0}", MyMatrix.Equals(mat1, mat2));
			
			mat1 = MyMatrix.Transpose(mat1);
			
			Console.WriteLine("Transpose matrix1:");
			Console.WriteLine(mat1.ToString());
		
			int[] i = { 1, 3 };
			int[] j = { 0, 1 };
			MyMatrix subMat = MyMatrix.SubMatrix(mat1, i, j);
			Console.WriteLine("Submatrix:");
			Console.WriteLine(subMat.ToString());
			
			Console.ReadKey();
		}
	}	
}